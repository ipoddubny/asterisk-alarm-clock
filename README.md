# Hotel Alarm Clock on Asterisk

1. Alarm clock calls are initiated using Asterisk call files (in /var/spool/asterisk/outgoing).
  - Modification time for each call file is set to the requested time the next day, so
    Asterisk does not execute the file immediately and waits for the specified time.
  - file names conform to the format "alarm_clock_" + EXTEN + ".call",
    (example: "alarm__clock_101.call"), so that deletion of the call file effectively
    cancels the alarm clock.
  - call files specify the channel string to call (`SIP/<extension number>`),
    and a position in the dialplan (context, extension and priority) that will
    handle the call.
2. Call files are managed by a script, `/etc/asterisk/scripts/alarm.sh`.
  - `alarm.sh set EXTEN TIME`, where EXTEN is an extension number
     and TIME is time in format "HH:MM". If an alarm clock has already been set for
     the extension, it gets overwritten with the new time.
  - `alarm.sh cancel EXTEN` cancels the alarm for the extension by deleting the call file.
3. The script `alarm.sh` is executed from the dialplan by
   the subroutines `sub_alarm_clock_set` and `sub_alarm_clock_cancel` using the `System`
   application.
  - `sub_alarm_clock_set` takes 2 arguments: extension number and time in format `HHMM` (without colon).
    - if the script has completed successfully, user is notified that the alarm clock has been set
      to go off at the specified time (using `Playback` and `SayUnixTime`).
    - if the script fails, the subroutine returns FAIL silently.
  - `sub_alarm_clock_cancel` takes one argument: extension number.
4. The subroutines are called from 2 contexts that contain feature codes for
   the frontdesk and for the rooms respectively.
   - front desk alarm clock feature codes extract the extension number from the dialed number (`${EXTEN:X:Y}`)
   - room alarm clock feature codes take the extension number from the caller's number (`${CALLERID(num)}`).

## SIP configuration, sip.conf

### General settings
```
[general]
udpbindaddr=0.0.0.0

allowguest=no
allowoverlap=no
callcounter=yes
```
Some basic settings:
 - SIP listens on all interfaces on UDP port 5060,
 - unauthorized calls are not allowed,
 - overlapped dialing is disabled,
 - call counters are enabled.

### Basic phone template
```
[phone](!)
type=friend
host=dynamic
disallow=all
allow=alaw
allow=g729
dtmfmode=rfc2833
```
All the phones inherit from this template. It contains basic settings:
- `type=friend`. It is both a "SIP user" and a "SIP peer",
   so it will be matched by section name and by IP address.
- `host=dynamic`. Phones register themselves on the Asterisk.
- `disallow` and `allow` set the list of codecs
- `dtmfmode` sets the DTMF transmission method


```
; front desk
[000](phone)
context=from-frontdesk
secret=BydnoshIr2quib6
```

Frontdesk phone number is 000, it inherits from the phone template and uses
the dialplan context "from-frontdesk" for handling calls.
`secret` specifies the password to authenticate incoming calls.


```
[room-phone](!,phone)
context=from-room
```
All the phones in the rooms use the common context "from-room",
so there is another template to avoid repetition.

```
; rooms
[101](room-phone)
secret=FawJesJiwyGrek5
[102](room-phone)
secret=ariphhowCiecov2
; ...
[514](room-phone)
secret=KocEaphNabBawb0
[515](room-phone)
secret=EvDanvuvdisyeg2
```

SIP peers for the rooms (101-115, ..., 501-515).

## Dialplan, extensions.conf

```
[general]
static=yes
writeprotect=yes

[globals]
ALARM_CLOCK_SCRIPT=/etc/asterisk/scripts/alarm.sh
```
The path to the alarm clock script is specified as a global variable to avoid repetition in the code below.

### Phone contexts
```
[from-rooms]

exten => 000,1,Dial(SIP/000) ; front desk
same => n,Hangup

exten => _*66*XXXX,1,Gosub(sub_alarm_clock_set,s,1(${CALLERID(num)},${EXTEN:4}))
same => n,Hangup

exten => **66,1,Gosub(sub_alarm_clock_cancel,s,1,(${CALLERID(num)}))
same => n,Hangup
```
Room phones can only dial the front desk (000) and set or cancel their alarm clocks.
The subroutines are explained below.

```
[from-frontdesk]

exten => _[1-5]XX,1,NoOp(Dialing room #${EXTEN})
same => n,Dial(SIP/${EXTEN})
same => n,Hangup

exten => _*66*XXXX*XXX,1,Gosub(sub_alarm_clock_set,s,1(${EXTEN:9},${EXTEN:4:4}))
same => n,Hangup

exten => _**66*XXX,1,Gosub(sub_alarm_clock_cancel,s,1,(${EXTEN:5}))
same => n,Hangup
```
The front desk manager can dial the rooms, and set or cancel their alarm clocks.

### Alarm clock

```
[sub_alarm_clock_set]
exten => s,1,NoOp(Setting alarm clock for ${ARG1} at ${ARG2:0:2}:${ARG2:2:2})
same => n,System(${ALARM_CLOCK_SCRIPT} set ${ARG1} ${ARG2:0:2}:${ARG2:2:2})
same => n,ExecIf($["${SYSTEMSTATUS}"!="SUCCESS"]?Return(FAIL))
same => n,Playback(alarm/set) ; "alarm clock is now set to go off at"
same => n,SayUnixTime(${STRPTIME(1970-01-01 ${ARG2:0:2}:${ARG2:2:2},,%Y-%m-%d %H:%M)},,"HM")
same => n,Return
```
System executes the script and sets the SYSTEMSTATUS variable.
If the result is not SUCCESS, then the subroutine returns failure.
Otherwise, the caller is played a message like "alarm clock is now set to go off at 07:04".


```
[sub_alarm_clock_cancel]
exten => s,1,NoOp(Cancelling alarm clock for ${ARG1})
same => n,System(${ALARM_CLOCK_SCRIPT} cancel ${ARG1})
same => n,ExecIf($["${SYSTEMSTATUS}"!="SUCCESS"]?Return(FAIL))
same => n,Playback(alarm/cancel) ; "alarm clock has been cancelled"
same => n,Return
```

```
[alarm_clock_originate_target]
exten => _X.,1,NoOp(Waking up ${EXTEN})
same => n,SayUnixTime(,,HM)    ; ex: oh six fourty five
same => n,Playback(tt-monkeys) ; alarm
same => n,Hangup
```
Originated calls land here, with extension equal to the room number that is now called.
The user is played current time followed by screaming monkeys. This of course needs to
be changed to use it for actual clients.

## The shell script
```sh
#!/bin/bash

# stop on errors
set -e

SPOOL_DIR=/var/spool/asterisk/outgoing
CALLFILE_PREFIX=alarm_clock_

usage() {
	cat <<EOF
Usage: $(basename $0) set <exten> <time>
       sets alarm clock for the extension at specified time tomorrow (HH:MM)

       $(basename $0) cancel <exten>
       cancels alarm clock for the extension
EOF
}

setAlarm() {
	EXTEN=$1
	TIME=$2
	TEMPFILE=`tempfile --suffix=.call`

	cat <<-EOF > $TEMPFILE
	Channel: SIP/$EXTEN
	Context: alarm_clock_originate_target
	Extension: $EXTEN
	Priority: 1
EOF
	touch -t `date +%Y%m%d%H%M --date="tomorrow $TIME"` $TEMPFILE
	mv -f $TEMPFILE "$SPOOL_DIR/${CALLFILE_PREFIX}${EXTEN}.call"
}

cancelAlarm() {
	EXTEN=$1

	rm "$SPOOL_DIR/${CALLFILE_PREFIX}${EXTEN}.call"
}


case "$1" in
	set)
		if [ $# -ne 3 ]; then
			usage
			exit 1
		fi
		setAlarm $2 $3
		;;

	cancel)
		if [ $# -ne 2 ]; then
			usage
			exit 1
		fi
		cancelAlarm $2
		;;

	*)
		usage
		;;
esac
```
