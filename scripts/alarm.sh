#!/bin/bash

# stop on errors
set -e

SPOOL_DIR=/var/spool/asterisk/outgoing
CALLFILE_PREFIX=alarm_clock_

usage() {
	cat <<EOF
Usage: $(basename $0) set <exten> <time>
       sets alarm clock for the extension at specified time tomorrow (HH:MM)

       $(basename $0) cancel <exten>
       cancels alarm clock for the extension
EOF
}

setAlarm() {
	EXTEN=$1
	TIME=$2
	TEMPFILE=`mktemp --suffix=.call`

	cat <<-EOF > $TEMPFILE
	Channel: SIP/$EXTEN
	Context: alarm_clock_originate_target
	Extension: $EXTEN
	Priority: 1
EOF
	touch -t `date +%Y%m%d%H%M --date="tomorrow $TIME"` $TEMPFILE
	mv -f $TEMPFILE "$SPOOL_DIR/${CALLFILE_PREFIX}${EXTEN}.call"
}

cancelAlarm() {
	EXTEN=$1

	rm "$SPOOL_DIR/${CALLFILE_PREFIX}${EXTEN}.call"
}


case "$1" in
	set)
		if [ $# -ne 3 ]; then
			usage
			exit 1
		fi
		setAlarm $2 $3
		;;

	cancel)
		if [ $# -ne 2 ]; then
			usage
			exit 1
		fi
		cancelAlarm $2
		;;

	*)
		usage
		;;
esac
